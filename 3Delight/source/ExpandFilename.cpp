#include "ExpandFilename.h"
#include <iomanip>
#include <sstream>

using namespace std;

bool Replace(std::string& data, std::string toSearch, std::string replaceStr)
{
	bool found = false;
	// Get the first occurrence
	size_t pos = data.find(toSearch);

	// Repeat till end is reached
	while (pos != std::string::npos) {
		found = true;
		// Replace this occurrence of Sub String
		data.replace(pos, toSearch.size(), replaceStr);
		// Get the next occurrence from the current position
		pos = data.find(toSearch, pos + replaceStr.size());
	}

	return found;
}

void ExpandFilename(std::string& filename,
					long frame,
					std::string document_path,
					std::string document_name,
					std::string aov)
{
	ostringstream ss;
	ss << frame;
	string framenumber = ss.str();
	ostringstream ss2;
	ss2 << std::setw(4) << std::setfill('0') << frame;
	string framenumber_padded = ss2.str();
	bool found_framenumber = false;
	Replace(filename, "<aov>", aov);
	Replace(filename, "<project>", document_path);
	Replace(filename, "<scene>", document_name);

	if (Replace(filename, "@", framenumber)) {
		found_framenumber = true;
	}

	if (Replace(filename, "#", framenumber_padded)) {
		found_framenumber = true;
	}

	if (!found_framenumber) {
		filename = filename + framenumber_padded;
	}
}