#include "BitmapDisplayDriver.h"
#include "IDs.h"
#include "c4d.h"

const void*
GetParameter(const char* name, unsigned n, const UserParameter parms[])
{
	for (unsigned i = 0; i < n; i++) {
		if (0 == strcmp(name, parms[i].name)) {
			return parms[i].value;
		}
	}

	return 0x0;
}

PtDspyError
BmpDspyImageQuery(PtDspyImageHandle image,
				  PtDspyQueryType type,
				  int size,
				  void* data)
{
	return PkDspyErrorNone;
}

PtDspyError
BmpDspyImageOpen(PtDspyImageHandle* image,
				 const char* drivername,
				 const char* filename,
				 int width,
				 int height,
				 int paramcount,
				 const UserParameter* parameters,
				 int formatcount,
				 PtDspyDevFormat* format,
				 PtFlagStuff* flags)
{
	VPBuffer** buffer_ptr =
		(VPBuffer**) GetParameter("BaseBitmap", paramcount, parameters);

	if (!buffer_ptr) {
		return PkDspyErrorBadParams;
	}

	VPBuffer* buffer = *buffer_ptr;
	*image = (void*) buffer;

	for (int i = 0; i < formatcount; i++) {
		// GePrint((String)format[i].name);
		format[i].type = PkDspyClassFloat;
	}

	return PkDspyErrorNone;
}

PtDspyError
BmpDspyImageData(PtDspyImageHandle image,
				 int xmin,
				 int xmax_plus_one,
				 int ymin,
				 int ymax_plus_one,
				 int entrysize,
				 const unsigned char* data)
{
	VPBuffer* buffer = (VPBuffer*) image;
	int W = xmax_plus_one - xmin;
	int skip;

	for (int y = ymin; y < ymax_plus_one; y++) {
		skip = (y - ymin) * entrysize * (W);
		buffer->SetLine(xmin, y, W, (void*) (&data[0] + skip), 32, true);
	}

	return PkDspyErrorNone;
}

PtDspyError
BmpDspyImageClose(PtDspyImageHandle image)
{
	return PkDspyErrorNone;
}