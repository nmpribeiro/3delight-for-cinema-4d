#define WIN32_LEAN_AND_MEAN
#include "nsi_dynamic.hpp"

class NSIAPI
{
public:
	~NSIAPI();

	static NSIAPI& Instance();
	static void DeleteInstance();

	NSI::DynamicAPI& API()
	{
		return m_api;
	}

private:
	NSIAPI();

	NSI::DynamicAPI m_api;
};
