#include "IDs.h"
#include "c4d.h"

class DL_HELP : public CommandData
{
public:
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_HELP::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	const maxon::String delight_url(
		"https://www.3delight.com/documentation/display/3C4/Introduction"_s);
	GeOpenHTML(delight_url);
	return true;
}

Bool RegisterHelp(void)
{
	return RegisterCommandPlugin(HELP_ID,
								 "Help..."_s,
								 PLUGINFLAG_HIDEPLUGINMENU,
								 0,
								 String(),
								 NewObjClear(DL_HELP));
}