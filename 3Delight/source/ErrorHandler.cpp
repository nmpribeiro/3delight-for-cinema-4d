#include "ErrorHandler.h"
#include <string>
#include "c4d.h"
#include "nsi.h"

void NSIErrorHandlerC4D(void* userdata, int level, int code, const char* message)
{
	const char* pre = (const char*) userdata;
	std::string buffer("3Delight");

	if (pre) {
		buffer += " (" + std::string(pre) + "): ";

	} else {
		buffer += ": ";
	}

	buffer += message;

	switch (level) {
	case NSIErrMessage:
		ApplicationOutput(buffer.c_str());
		break;

	case NSIErrInfo:
		ApplicationOutput(buffer.c_str());
		break;

	case NSIErrWarning:
		ApplicationOutput(buffer.c_str());
		break;

	default:
	case NSIErrError:
		ApplicationOutput(buffer.c_str());
		break;
	}
}