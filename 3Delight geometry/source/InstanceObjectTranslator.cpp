#include "InstanceObjectTranslator.h"
#include <vector>
#include "DL_TypeConversions.h"
#include "lib_instanceobject.h"
#include "nsi.hpp"

bool InstanceObjectTranslator::Accept(BaseList2D* C4DNode)
{
	BaseObject* obj = (BaseObject*) C4DNode;
	BaseContainer* data = obj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_SINGLEINSTANCE || mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE) {
		return true;
	}

	return false;
}

void InstanceObjectTranslator::CreateNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	InstanceObject* Iobj = (InstanceObject*) C4DNode;
	BaseContainer* data = Iobj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE) {
		// Create transform nodes for multi-instances
		NSI::Context& ctx(parser->GetContext());
		UInt n_instances = Iobj->GetInstanceCount();

		for (UInt i = 0; i < n_instances; i++) {
			std::string instance_transform_handle =
				std::string(Handle) + "instance_transform" + std::to_string(i);
			ctx.Create(instance_transform_handle, "transform");
			ctx.Connect(
				instance_transform_handle, "", ParentTransformHandle, "objects");
		}
	}
}

void InstanceObjectTranslator::ConnectNSINodes(const char* Handle,
		const char* ParentTransformHandle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	InstanceObject* Iobj = (InstanceObject*) C4DNode;
	BaseContainer* data = Iobj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);
	BaseObject* Refobj = Iobj->GetReferenceObject(doc);

	if (!Refobj) {
		return;
	}

	std::string ref_handle = "X0_" + parser->GetHandleName(Refobj);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_SINGLEINSTANCE) { // single instance
		// mode
		ctx.Connect(ref_handle, "", ParentTransformHandle, "objects");

	} else if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE) { // multi
		// instance
		// mode
		UInt n_instances = Iobj->GetInstanceCount();

		for (UInt i = 0; i < n_instances; i++) {
			std::string instance_transform_handle =
				std::string(Handle) + "instance_transform" + std::to_string(i);
			ctx.Connect(ref_handle, "", instance_transform_handle, "objects");
		}
	}
}

void InstanceObjectTranslator::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser)
{
	InstanceObject* Iobj = (InstanceObject*) C4DNode;
	BaseContainer* data = Iobj->GetDataInstance();
	long mode = data->GetInt32(INSTANCEOBJECT_RENDERINSTANCE_MODE);

	if (mode == INSTANCEOBJECT_RENDERINSTANCE_MODE_MULTIINSTANCE) {
		// Sample transforms for multi-instances
		NSI::Context& ctx(parser->GetContext());
		Matrix parent_transform = Iobj->GetMg();
		Matrix parent_transform_inverse = ~parent_transform;
		UInt n_instances = Iobj->GetInstanceCount();

		for (UInt i = 0; i < n_instances; i++) {
			std::string instance_transform_handle =
				std::string(Handle) + "instance_transform" + std::to_string(i);
			Matrix instance_transform = Iobj->GetInstanceMatrix(i);
			// Instance matrix is in global coordinates
			// multiply by inverse of parent transform to compensate
			instance_transform = parent_transform_inverse * instance_transform;
			std::vector<double> v = MatrixToNSIMatrix(instance_transform);
			NSI::Argument xform("transformationmatrix");
			xform.SetType(NSITypeDoubleMatrix);
			xform.SetValuePointer((void*) &v[0]);
			ctx.SetAttributeAtTime(
				instance_transform_handle, info->sample_time, (xform));
		}
	}
}