#pragma once

#include "AreaLightTranslator.h"
#include "DLDecayFilterTranslator.h"
#include "DLGoboFilterTranslator.h"
#include "DL_API.h"
#include "DirectionalLightTranslator.h"
#include "IDs.h"
#include "IncandescenceLightTranslator.h"
#include "LightGroupTranslator.h"
#include "PointLightTranslator.h"
#include "SpotLightTranslator.h"
#include "c4d.h"
#include "c4dLightTranslator.h"

// Forward declaration
bool RegisterAreaLight(void);
bool RegisterDirectionalLight(void);
bool RegisterPointLight(void);
bool RegisterIncandescenceLight(void);
bool RegisterSpotLight(void);
bool RegisterCustomLightFilters(void);
bool RegisterGoboFilter(void);
bool RegisterDecayFilter(void);
bool RegisterLightGroup(void);
bool Register_LightShape(void);
