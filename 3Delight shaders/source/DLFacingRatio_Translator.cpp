#include "DLFacingRatio_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_facing_ratio.h"

Delight_FacingRatio::Delight_FacingRatio()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlFacingRatio.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[FACING_RATIO_COLOR_EDGE] = std::make_pair("", "color_edge");
	m_ids_to_names[FACING_RATIO_COLOR_EDGE_SHADER] =
		std::make_pair("outColor", "color_edge");
	m_ids_to_names[FACING_RATIO_COLOR_CENTER] =
		std::make_pair("", "color_center");
	m_ids_to_names[FACING_RATIO_COLOR_CENTER_SHADER] =
		std::make_pair("outColor", "color_center");
	m_ids_to_names[FACING_RATIO_BIAS] = std::make_pair("", "bias");
	m_ids_to_names[FACING_RATIO_BIAS_SHADER] =
		std::make_pair("outColor[0]", "bias");
	m_ids_to_names[FACING_RATIO_CONTRAST] = std::make_pair("", "contrast");
	m_ids_to_names[FACING_RATIO_CONTRAST_SHADER] =
		std::make_pair("outColor[0]", "contrast");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
}
