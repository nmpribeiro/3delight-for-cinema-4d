#include "DL_Matpreview.h"
#include "customgui_matpreview.h"

void DL_SetDefaultMatpreview(BaseMaterial* material)
{
	GeData d;

	if (material->GetParameter(
				DescLevel(MATERIAL_PREVIEW), d, DESCFLAGS_GET::NONE)) {
		MaterialPreviewData* mpd =
			(MaterialPreviewData*) d.GetCustomDataType(CUSTOMDATATYPE_MATPREVIEW);

		if (mpd) {
			mpd->SetPreviewType(MatPreviewUser);
			mpd->SetUserPreviewSceneName(String("DL_Sphere"));
			material->SetParameter(
				DescLevel(MATERIAL_PREVIEW), d, DESCFLAGS_SET::NONE);
		}
	}
}
