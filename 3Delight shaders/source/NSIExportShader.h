#pragma once
#include <map>
#include "DL_TranslatorPlugin.h"

class NSI_Export_Shader : public DL_TranslatorPlugin
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void ConnectNSINodes(const char* Handle,
								 const char* ParentTransformHandle,
								 BaseList2D* C4DNode,
								 BaseDocument* doc,
								 DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);

protected:
	// std::string m_shader_handle;
	std::map<int, std::pair<std::string, std::string>> m_ids_to_names;
	std::string m_shaderpath;
	bool is_3delight_shader = false;
	bool has_uv_connection = true;
};
