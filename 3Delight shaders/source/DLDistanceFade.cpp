#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_distance_fade.h"
#include "TextureUI_Functions.h"
#include "IDs.h"

static void InitRamp(BaseContainer &bc, Int32 id)
{
	GeData data(CUSTOMDATATYPE_SPLINE, DEFAULTVALUE);
	SplineData* spline = (SplineData*)data.GetCustomDataType(CUSTOMDATATYPE_SPLINE);
	if (spline)
	{
		spline->MakeLinearSplineLinear(2); //Make Linear Spline

		Int32 count = spline->GetKnotCount();
		if (count == 2)
		{
			CustomSplineKnot* knot0 = spline->GetKnot(0);
			if (knot0)
			{
				knot0->vPos = Vector(0, 0, 0);
			}

			CustomSplineKnot* knot1 = spline->GetKnot(1);
			if (knot1)
			{
				knot1->vPos = Vector(1, 1, 0);
			}
		}
	}
	bc.SetData(id, data);
}

class DLDistanceFade : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Bool GetDDescription(GeListNode *node, Description *description, DESCFLAGS_DESC &flags);
	virtual Bool Message(GeListNode *node, Int32 type, void *data);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void) { return NewObjClear(DLDistanceFade); }
};

Bool DLDistanceFade::Init(GeListNode* node)
{
	BaseContainer* data = ((BaseShader*)node)->GetDataInstance();
	data->SetFloat(MINIMUM_DISTANCE, 0);
	data->SetFloat(MAXIMUM_DISTANCE, 10);
	data->SetFloat(FOLLOW_CAM_FOV, 0);
	InitRamp(*data, FADE_CURVE);
	return true;
}



Bool DLDistanceFade::GetDDescription(GeListNode *node, Description *description, DESCFLAGS_DESC &flags)
{
	description->LoadDescription(DL_DISTANCE_FADE);
	flags |= DESCFLAGS_DESC::LOADED;

	BaseContainer* dldata = ((BaseObject*)node)->GetDataInstance();

	HideAndShowTextures(MINIMUM_DISTANCE_GROUP_PARAM, MINIMUM_DISTANCE, MINIMUM_DISTANCE_SHADER, MINIMUM_DISTANCE_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(MAXIMUM_DISTANCE_GROUP_PARAM, MAXIMUM_DISTANCE, MAXIMUM_DISTANCE_SHADER, MAXIMUM_DISTANCE_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(FOLLOW_CAM_FOV_GROUP_PARAM, FOLLOW_CAM_FOV, FOLLOW_CAM_FOV_SHADER, FOLLOW_CAM_FOV_SHADER_TEMP, node, description, dldata);

	return TRUE;
}



Bool DLDistanceFade::Message(GeListNode *node, Int32 type, void *data)
{
	if (!node)
		return false;

	BaseContainer* dldata = ((BaseMaterial*)node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP)
	{
		DescriptionPopup *dp = (DescriptionPopup*)data;
		int clicked_button_id = dp->_descId[0].id;
		switch (clicked_button_id)
		{
		case POPUP_MINIMUM_DISTANCE:
			FillPopupMenu(dldata, dp, MINIMUM_DISTANCE_GROUP_PARAM);
			break;

		case POPUP_MAXIMUM_DISTANCE:
			FillPopupMenu(dldata, dp, MAXIMUM_DISTANCE_GROUP_PARAM);
			break;

		case POPUP_FOLLOW_CAM_FOV:
			FillPopupMenu(dldata, dp, FOLLOW_CAM_FOV_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}


Vector DLDistanceFade::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterDistanceFadeTexture(void)
{
	return RegisterShaderPlugin(DL_DISTANCE_FADE, "Distance Fade"_s, 0, DLDistanceFade::Alloc, "dl_distance_fade"_s, 0);
}
