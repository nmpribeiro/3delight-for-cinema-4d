#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_metal.h"
class DL_Metal : public MaterialData
{
	INSTANCEOF(DL_Metal, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Metal);
	}
};

Bool DL_Metal::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetFloat(COATING_LAYER_THICKNESS, 0);
	Vector coating_color(1, 0.5, 0.1);
	data->SetVector(COATING_LAYER_COLOR,
					HSVToRGB(Vector(26.667 / 360.0, 0.9, 1.0)));
	data->SetFloat(COATING_LAYER_ROUGHNESS, 0);
	data->SetFloat(COATING_LAYER_SPECULAR_LEVEL, 0.5);
	data->SetVector(METAL_LAYER_COLOR,
					HSVToRGB(Vector(9.071 / 360.0, 0.431, 0.947)));
	data->SetVector(METAL_LAYER_EDGE_COLOR,
					HSVToRGB(Vector(15.914 / 360.0, 0.204, 1.0)));
	data->SetFloat(METAL_LAYER_ROUGHNESS, 0.2);
	data->SetFloat(METAL_LAYER_ANISOTROPY, 0);
	Vector anisotropy_direction(0.5, 1, 0);
	data->SetVector(METAL_LAYER_ANISOTROPY_DIRECTION,
					HSVToRGB(Vector(90 / 360.0, 1.0, 1.0)));
	data->SetFloat(METAL_LAYER_OPACITY, 1);
	data->SetFloat(METAL_LAYER_THIN_THICKNESS, 0.0);
	data->SetFloat(METAL_LAYER_THIN_IOR, 2.5);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	data->SetInt32(BUMP_LAYERS_AFFECTED, AFFECT_BOTH_LAYERS);
	// color = data->GetVector(SIMPLEMATERIAL_COLOR);
	return true;
}

Bool DL_Metal::GetDEnabling(GeListNode* node,
							const DescID& id,
							const GeData& t_data,
							DESCFLAGS_ENABLE flags,
							const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case COATING_LAYER_COLOR:
	case COATING_LAYER_ROUGHNESS:
	case COATING_LAYER_SPECULAR_LEVEL:
	case COATING_LAYER_COLOR_SHADER:
	case COATING_LAYER_ROUGHNESS_SHADER:
	case COATING_LAYER_SPECULAR_LEVEL_SHADER:
	case POPUP_COATING_LAYER_COLOR:
	case POPUP_COATING_LAYER_ROUGHNESS:
	case POPUP_COATING_LAYER_SPECULAR_LEVEL:
		return (
				   dldata->GetFloat(COATING_LAYER_THICKNESS) > 0 || (dldata->GetLink(COATING_LAYER_THICKNESS_SHADER, doc) != nullptr));
		break;

	default:
		break;
	}

	return true;
}

Bool DL_Metal::GetDDescription(GeListNode* node,
							   Description* description,
							   DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_METAL);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(COATING_LAYER_THICKNESS_GROUP_PARAM,
						COATING_LAYER_THICKNESS,
						COATING_LAYER_THICKNESS_SHADER,
						COATING_LAYER_THICKNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COATING_LAYER_COLOR_GROUP_PARAM,
						COATING_LAYER_COLOR,
						COATING_LAYER_COLOR_SHADER,
						COATING_LAYER_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COATING_LAYER_ROUGHNESS_GROUP_PARAM,
						COATING_LAYER_ROUGHNESS,
						COATING_LAYER_ROUGHNESS_SHADER,
						COATING_LAYER_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COATING_LAYER_SPECULAR_LEVEL_GROUP_PARAM,
						COATING_LAYER_SPECULAR_LEVEL,
						COATING_LAYER_SPECULAR_LEVEL_SHADER,
						COATING_LAYER_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_COLOR_GROUP_PARAM,
						METAL_LAYER_COLOR,
						METAL_LAYER_COLOR_SHADER,
						METAL_LAYER_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_EDGE_COLOR_GROUP_PARAM,
						METAL_LAYER_EDGE_COLOR,
						METAL_LAYER_EDGE_COLOR_SHADER,
						METAL_LAYER_EDGE_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_ROUGHNESS_GROUP_PARAM,
						METAL_LAYER_ROUGHNESS,
						METAL_LAYER_ROUGHNESS_SHADER,
						METAL_LAYER_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_ANISOTROPY_GROUP_PARAM,
						METAL_LAYER_ANISOTROPY,
						METAL_LAYER_ANISOTROPY_SHADER,
						METAL_LAYER_ANISOTROPY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_ANISOTROPY_DIRECTION_GROUP_PARAM,
						METAL_LAYER_ANISOTROPY_DIRECTION,
						METAL_LAYER_ANISOTROPY_DIRECTION_SHADER,
						METAL_LAYER_ANISOTROPY_DIRECTION_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_OPACITY_GROUP_PARAM,
						METAL_LAYER_OPACITY,
						METAL_LAYER_OPACITY_SHADER,
						METAL_LAYER_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_THIN_THICKNESS_GROUP_PARAM,
						METAL_LAYER_THIN_THICKNESS,
						METAL_LAYER_THIN_THICKNESS_SHADER,
						METAL_LAYER_THIN_THICKNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(METAL_LAYER_THIN_IOR_GROUP_PARAM,
						METAL_LAYER_THIN_IOR,
						METAL_LAYER_THIN_IOR_SHADER,
						METAL_LAYER_THIN_IOR_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_Metal::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_COATING_LAYER_THICKNESS:
			FillPopupMenu(dldata, dp, COATING_LAYER_THICKNESS_GROUP_PARAM);
			break;

		case POPUP_COATING_LAYER_COLOR:
			FillPopupMenu(dldata, dp, COATING_LAYER_COLOR_GROUP_PARAM);
			break;

		case POPUP_COATING_LAYER_ROUGHNESS:
			FillPopupMenu(dldata, dp, COATING_LAYER_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_COATING_LAYER_SPECULAR_LEVEL:
			FillPopupMenu(dldata, dp, COATING_LAYER_SPECULAR_LEVEL_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_COLOR:
			FillPopupMenu(dldata, dp, METAL_LAYER_COLOR_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_EDGE_COLOR:
			FillPopupMenu(dldata, dp, METAL_LAYER_EDGE_COLOR_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_ROUGHNESS:
			FillPopupMenu(dldata, dp, METAL_LAYER_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_ANISOTROPY:
			FillPopupMenu(dldata, dp, METAL_LAYER_ANISOTROPY_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_ANISOTROPY_DIRECTION:
			FillPopupMenu(dldata, dp, METAL_LAYER_ANISOTROPY_DIRECTION_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_OPACITY:
			FillPopupMenu(dldata, dp, METAL_LAYER_OPACITY_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_THIN_THICKNESS:
			FillPopupMenu(dldata, dp, METAL_LAYER_THIN_THICKNESS_GROUP_PARAM);
			break;

		case POPUP_METAL_LAYER_THIN_IOR:
			FillPopupMenu(dldata, dp, METAL_LAYER_THIN_IOR_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_Metal::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_Metal::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLMetal(void)
{
	return RegisterMaterialPlugin(
			   DL_METAL, "Metal"_s, PLUGINFLAG_HIDE, DL_Metal::Alloc, "dl_metal"_s, 0);
}
