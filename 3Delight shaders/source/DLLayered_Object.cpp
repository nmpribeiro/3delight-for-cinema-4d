#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Layered_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Layered_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_LAYERED, doc);
	return true;
}

Bool Register_Layered_Object(void)
{
	DL_Layered_command* new_layered = NewObjClear(DL_Layered_command);
	RegisterCommandPlugin(DL_LAYERED_COMMAND,
						  "Layered"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlLayeredMaterial_200.png"_s),
						  String("Assign new Layered"),
						  NewObjClear(DL_Layered_command));

	if (RegisterCommandPlugin(DL_LAYERED_SHELF_COMMAND,
							  "Layered"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlLayeredMaterial_200.png"_s),
							  String("Assign new Layered Material"),
							  new_layered)) {
		new_layered->shelf_used = 1;
	}

	return true;
}
