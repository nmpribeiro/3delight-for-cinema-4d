#include "DLShadowMatte_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_shadow_matte.h"

Delight_ShadowMatte::Delight_ShadowMatte()
{
	is_3delight_material = true;
	has_uv_connection = false;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlShadowMatte.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[SHADOW_MATTE_COLOR] = std::make_pair("", "shadow_color");
	m_ids_to_names[SHADOW_MATTE_COLOR_SHADER] = std::make_pair("outColor", "shadow_color");
	m_ids_to_names[SHADOW_MATTE_OPACITY] = std::make_pair("", "shadow_opacity");
	m_ids_to_names[SHADOW_MATTE_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "shadow_opacity");
	m_ids_to_names[SHADOW_DIFFUSE_COLOR] = std::make_pair("", "diffuse_color");
	m_ids_to_names[SHADOW_DIFFUSE_COLOR] =
		std::make_pair("outColor", "diffuse_color");
	m_ids_to_names[SHADOW_DIFFUSE_INTENSITY] = std::make_pair("", "diffuse_intensity");
	m_ids_to_names[SHADOW_DIFFUSE_INTENSITY_SHADER] =
		std::make_pair("outColor[0]", "diffuse_intensity");
	m_ids_to_names[SHADOW_DIFFUSE_ROUGHNESS] = std::make_pair("", "diffuse_roughness");
	m_ids_to_names[SHADOW_DIFFUSE_ROUGHNESS_SHADER] = std::make_pair("outColor[0]", "diffuse_roughness");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
