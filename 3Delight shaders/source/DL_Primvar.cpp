#include "dl_primvar.h"
#include "IDs.h"
#include "c4d.h"

class DL_Primvar : public ShaderData
{
public:
	virtual Bool Init(GeListNode* node);
	virtual Vector Output(BaseShader* sh, ChannelData* sd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_Primvar);
	}
};

Bool DL_Primvar::Init(GeListNode* node)
{
	// BaseContainer* data = ((BaseShader*)node)->GetDataInstance();
	return true;
}

Vector
DL_Primvar::Output(BaseShader* sh, ChannelData* sd)
{
	return Vector(0, 0, 0);
}

Bool RegisterDLPrimvar(void)
{
	return RegisterShaderPlugin(DL_PRIMVAR,
								"Primitive Attribute"_s,
								0,
								DL_Primvar::Alloc,
								"dl_primvar"_s,
								0);
}
