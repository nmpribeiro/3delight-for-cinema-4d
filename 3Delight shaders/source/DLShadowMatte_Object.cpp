#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_ShadowMatte_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_ShadowMatte_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_SHADOW_MATTE, doc);
	return true;
}

Bool Register_ShadowMatte_Object(void)
{
	DL_ShadowMatte_command* new_shadow = NewObjClear(DL_ShadowMatte_command);
	RegisterCommandPlugin(DL_SHADOWMATTE_COMMAND,
		"Shadow Matte"_s,
		PLUGINFLAG_HIDEPLUGINMENU,
		0,
		String("Assign new Shadow Matte"),
		NewObjClear(DL_ShadowMatte_command));

	if (RegisterCommandPlugin(DL_SHADOW_MATTE_SHELF_COMMAND,
		"Shadow Matte"_s,
		PLUGINFLAG_HIDEPLUGINMENU,
		0,
		String("Assign new Shadow Matte Material"),
		new_shadow)) {
		new_shadow->shelf_used = 1;
	}

	return true;
}