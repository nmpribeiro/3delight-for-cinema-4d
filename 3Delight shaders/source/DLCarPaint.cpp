#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "dl_car_paint.h"

class DL_CarPaint : public MaterialData
{
	INSTANCEOF(DL_CarPaint, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_CarPaint);
	}
};

Bool DL_CarPaint::Init(GeListNode* node)
{
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetFloat(COATING_LAYER_THICKNESS, 0.050);
	Vector coating_color = HSVToRGB(Vector(39.231, 0, 0.9));
	data->SetVector(COATING_LAYER_COLOR, coating_color);
	data->SetFloat(COATING_LAYER_ROUGHNESS, 0.050);
	data->SetFloat(COATING_LAYER_SPECULAR_LEVEL, 0.5);
	data->SetFloat(FLAKES_DENSITY, 0.3);
	Vector flakes_color = HSVToRGB(Vector(39.231, 0, 1));
	data->SetVector(FLAKES_COLOR, flakes_color);
	data->SetFloat(FLAKES_ROUGHNESS, 0.3);
	data->SetFloat(FLAKES_SCALE, 0.1);
	data->SetFloat(FLAKES_RANDOMNESS, 0.3);
	Vector base_color = HSVToRGB(Vector(39.231, 0, 0.8));
	data->SetVector(BASE_LAYER_COLOR, base_color);
	data->SetFloat(BASE_LAYER_ROUGHNESS, 0.3);
	data->SetFloat(BASE_LAYER_SPECULAR_LEVEL, 0.5);
	data->SetFloat(BASE_LAYER_METALLIC, 0);
	data->SetInt32(BUMP_TYPE, BUMP_MAP);
	data->SetFloat(BUMP_INTENSITY, 1);
	data->SetInt32(BUMP_LAYERS_AFFECTED, AFFECT_BOTH_LAYERS);
	return true;
}

Bool DL_CarPaint::GetDEnabling(GeListNode* node,
							   const DescID& id,
							   const GeData& t_data,
							   DESCFLAGS_ENABLE flags,
							   const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();

	switch (id[0].id) {
	case COATING_LAYER_THICKNESS:
		return bool(dldata->GetLink(COATING_LAYER_THICKNESS_SHADER, doc)) == false;
		break;

	case COATING_LAYER_COLOR:
		return (dldata->GetFloat(COATING_LAYER_THICKNESS) > 0 && !dldata->GetLink(COATING_LAYER_COLOR_SHADER, doc)) == true;
		break;

	case COATING_LAYER_ROUGHNESS:
		return (dldata->GetFloat(COATING_LAYER_THICKNESS) > 0 && !dldata->GetLink(COATING_LAYER_ROUGHNESS_SHADER, doc)) == true;
		break;

	case COATING_LAYER_SPECULAR_LEVEL:
		return (dldata->GetFloat(COATING_LAYER_THICKNESS) > 0 && !dldata->GetLink(COATING_LAYER_SPECULAR_LEVEL_SHADER, doc)) == true;
		break;

	case BASE_LAYER_COLOR:
		return bool(dldata->GetLink(BASE_LAYER_COLOR_SHADER,
									GetActiveDocument()))
			   == false;

	case BASE_LAYER_ROUGHNESS:
		return bool(dldata->GetLink(BASE_LAYER_ROUGHNESS_SHADER,
									GetActiveDocument()))
			   == false;

	case BASE_LAYER_SPECULAR_LEVEL:
		return bool(dldata->GetLink(BASE_LAYER_SPECULAR_LEVEL_SHADER,
									GetActiveDocument()))
			   == false;

	case BASE_LAYER_METALLIC:
		return bool(dldata->GetLink(BASE_LAYER_METALLIC_SHADER,
									GetActiveDocument()))
			   == false;

	case COATING_LAYER_COLOR_SHADER:
	case COATING_LAYER_ROUGHNESS_SHADER:
	case COATING_LAYER_SPECULAR_LEVEL_SHADER:
		return dldata->GetFloat(COATING_LAYER_THICKNESS) > 0;
		break;

	default:
		break;
	}

	return true;
}

Bool DL_CarPaint::GetDDescription(GeListNode* node,
								  Description* description,
								  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_CARPAINT);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(THICKNESS_GROUP_PARAM,
						COATING_LAYER_THICKNESS,
						COATING_LAYER_THICKNESS_SHADER,
						COATING_LAYER_THICKNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(COLOR_GROUP_PARAM,
						COATING_LAYER_COLOR,
						COATING_LAYER_COLOR_SHADER,
						COATING_LAYER_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(ROUGHNESS_GROUP_PARAM,
						COATING_LAYER_ROUGHNESS,
						COATING_LAYER_ROUGHNESS_SHADER,
						COATING_LAYER_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(SPECULAR_GROUP_PARAM,
						COATING_LAYER_SPECULAR_LEVEL,
						COATING_LAYER_SPECULAR_LEVEL_SHADER,
						COATING_LAYER_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_DENSITY_GROUP_PARAM,
						FLAKES_DENSITY,
						FLAKES_DENSITY_SHADER,
						FLAKES_DENSITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_COLOR_GROUP_PARAM,
						FLAKES_COLOR,
						FLAKES_COLOR_SHADER,
						FLAKES_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_ROUGHNESS_GROUP_PARAM,
						FLAKES_ROUGHNESS,
						FLAKES_ROUGHNESS_SHADER,
						FLAKES_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_SCALE_GROUP_PARAM,
						FLAKES_SCALE,
						FLAKES_SCALE_SHADER,
						FLAKES_SCALE_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(FLAKES_RANDOMNESS_GROUP_PARAM,
						FLAKES_RANDOMNESS,
						FLAKES_RANDOMNESS_SHADER,
						FLAKES_RANDOMNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BASE_COLOR_GROUP_PARAM,
						BASE_LAYER_COLOR,
						BASE_LAYER_COLOR_SHADER,
						BASE_LAYER_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BASE_ROUGHNESS_GROUP_PARAM,
						BASE_LAYER_ROUGHNESS,
						BASE_LAYER_ROUGHNESS_SHADER,
						BASE_LAYER_ROUGHNESS_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BASE_SPECULAR_GROUP_PARAM,
						BASE_LAYER_SPECULAR_LEVEL,
						BASE_LAYER_SPECULAR_LEVEL_SHADER,
						BASE_LAYER_SPECULAR_LEVEL_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(BASE_METALLIC_GROUP_PARAM,
						BASE_LAYER_METALLIC,
						BASE_LAYER_METALLIC_SHADER,
						BASE_LAYER_METALLIC_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_CarPaint::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseDocument* doc = GetActiveDocument();
	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (dldata->GetLink(COATING_LAYER_THICKNESS_SHADER, doc)) {
		dldata->SetFloat(COATING_LAYER_THICKNESS, 0.5);
	}

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_COLOR:
			FillPopupMenu(dldata, dp, COLOR_GROUP_PARAM);
			break;

		case POPUP_THICKNESS:
			FillPopupMenu(dldata, dp, THICKNESS_GROUP_PARAM);
			break;

		case POPUP_ROUGHNESS:
			FillPopupMenu(dldata, dp, ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_SPECULAR:
			FillPopupMenu(dldata, dp, SPECULAR_GROUP_PARAM);
			break;

		case POPUP_BASECOLOR:
			FillPopupMenu(dldata, dp, BASE_COLOR_GROUP_PARAM);
			break;

		case POPUP_BASEROUGHNESS:
			FillPopupMenu(dldata, dp, BASE_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_BASESPECULAR:
			FillPopupMenu(dldata, dp, BASE_SPECULAR_GROUP_PARAM);
			break;

		case POPUP_BASEMETALLIC:
			FillPopupMenu(dldata, dp, BASE_METALLIC_GROUP_PARAM);
			break;

		case POPUP_FLAKES_DENSITY:
			FillPopupMenu(dldata, dp, FLAKES_DENSITY_GROUP_PARAM);
			break;

		case POPUP_FLAKES_COLOR:
			FillPopupMenu(dldata, dp, FLAKES_COLOR_GROUP_PARAM);
			break;

		case POPUP_FLAKES_ROUGHNESS:
			FillPopupMenu(dldata, dp, FLAKES_ROUGHNESS_GROUP_PARAM);
			break;

		case POPUP_FLAKES_SCALE:
			FillPopupMenu(dldata, dp, FLAKES_SCALE_GROUP_PARAM);
			break;

		case POPUP_FLAKES_RANDOMNESS:
			FillPopupMenu(dldata, dp, FLAKES_RANDOMNESS_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_CarPaint::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_CarPaint::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLCarPaint(void)
{
	return RegisterMaterialPlugin(DL_CARPAINT,
								  "Car Paint"_s,
								  PLUGINFLAG_HIDE,
								  DL_CarPaint::Alloc,
								  "dl_car_paint"_s,
								  0);
}
