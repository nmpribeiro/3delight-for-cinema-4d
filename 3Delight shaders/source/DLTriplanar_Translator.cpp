#include "DLTriplanar_Translator.h"
#include "3DelightEnvironment.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_triplanar.h"
#include "nsi.hpp"

Delight_Triplanar::Delight_Triplanar()
{
	is_3delight_shader = true;
	has_uv_connection = false;
	// Passing only the parameters which support shader connections.
	m_ids_to_names[PLANE_SOFTNESS_SHADER] = std::make_pair("", "blend_softness");
	m_ids_to_names[PLANE_SOFTNESS_SHADER_TEMP] =
		std::make_pair("outColor[0]", "blend_softness");
	m_ids_to_names[PLANE_NOISE_INTENSITY_SHADER] =
		std::make_pair("", "blend_noise_intensity");
	m_ids_to_names[PLANE_NOISE_INTENSITY_SHADER_TEMP] =
		std::make_pair("outColor[0]", "blend_noise_intensity");
	m_ids_to_names[PLANE_NOISE_SCALE_SHADER] =
		std::make_pair("", "blend_noise_scale");
	m_ids_to_names[PLANE_NOISE_SCALE_SHADER_TEMP] =
		std::make_pair("outColor[0]", "blend_noise_scale");
	m_ids_to_names[TILE_REMOVAL_SOFTNESS_SHADER] =
		std::make_pair("", "tile_softness");
	m_ids_to_names[TILE_REMOVAL_SOFTNESS_SHADER_TEMP] =
		std::make_pair("outColor[0]", "tile_softness");
	m_ids_to_names[TILE_REMOVAL_OFFSET_SHADER] =
		std::make_pair("", "tile_offset");
	m_ids_to_names[TILE_REMOVAL_OFFSET_SHADER_TEMP] =
		std::make_pair("outColor[0]", "tile_offset");
	m_ids_to_names[TILE_REMOVAL_ROTATION_SHADER] =
		std::make_pair("", "tile_rotation");
	m_ids_to_names[TILE_REMOVAL_ROTATION_SHADER_TEMP] =
		std::make_pair("outColor[0]", "tile_rotation");
	m_ids_to_names[TILE_REMOVAL_SCALE_SHADER] = std::make_pair("", "tile_scale");
	m_ids_to_names[TILE_REMOVAL_SCALE_SHADER_TEMP] =
		std::make_pair("outColor[0]", "tile_scale");
}

// Use this function to get the Text value from the Dropdown (CYCLE)
std::string
getDropNames(Description* const description, Int32 group_id, Int32 SelectedItem)
{
	const DescID* singleid = description->GetSingleDescID();
	const DescID cid = DescLevel(group_id, DTYPE_LONG, 0);
	std::string selected_name = "";

	if (!singleid || cid.IsPartOf(*singleid, nullptr)) {
		AutoAlloc<AtomArray> arr;
		BaseContainer* selectionParameter =
			description->GetParameterI(DescLevel(group_id, DTYPE_LONG, 0), arr);

		if (selectionParameter != nullptr) {
			BaseContainer* items =
				selectionParameter->GetContainerInstance(DESC_CYCLE);

			if (items != nullptr) {
				selected_name =
					items->GetData(SelectedItem).GetString().GetCStringCopy();
			}
		}
	}

	if (selected_name == "None") {
		selected_name = "";
	}

	return selected_name;
}

void Delight_Triplanar::CreateNSINodes(const char* Handle,
									   const char* ParentTransformHandle,
									   BaseList2D* C4DNode,
									   BaseDocument* doc,
									   DL_SceneParser* parser)
{
	NSI::Context& ctx(parser->GetContext());
	m_handle = (std::string) Handle;
	ctx.Create(m_handle, "shader");
	BaseList2D* baselist = (BaseList2D*) C4DNode;
	BaseContainer* data = baselist->GetDataInstance();
	AutoAlloc<Description> description;

	if (description == nullptr) {
		return;
	}

	if (!baselist->GetDescription(description, DESCFLAGS_DESC::NONE)) {
		return;
	}

	int triplanar_mode = data->GetInt32(SHADER_MODE);
	std::string color_texture = "";
	Filename color_texture_file = data->GetFilename(COLOR_TEXTURE);

	if (color_texture_file.IsPopulated()) {
		color_texture = StringToStdString(color_texture_file.GetString());
	}

	std::string float_texture = "";
	Filename float_texture_file = data->GetFilename(FLOAT_COLOR_TEXTURE);

	if (float_texture_file.IsPopulated()) {
		float_texture = StringToStdString(float_texture_file.GetString());
	}

	std::string height_texture = "";
	Filename height_texture_file = data->GetFilename(HEIGHT_COLOR_TEXTURE);

	if (height_texture_file.IsPopulated()) {
		height_texture = StringToStdString(height_texture_file.GetString());
	}

	/*
	            Here we get the selected ID for the grids but we need the text
	            that it contains. Get String won't work as dropdown can be only
	            of LONG type
	    */
	int color_space = data->GetInt32(COLOR_SPACE);
	int float_color_space = data->GetInt32(FLOAT_COLOR_SPACE);
	int height_color_space = data->GetInt32(HEIGHT_COLOR_SPACE);
	std::string color_space_text =
		getDropNames(description, COLOR_SPACE, color_space);
	std::string float_color_space_text =
		getDropNames(description, FLOAT_COLOR_SPACE, float_color_space);
	std::string height_color_space_text =
		getDropNames(description, HEIGHT_COLOR_SPACE, height_color_space);
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlTriplanar.oso";
	NSI::ArgumentList args;
	args.Add(new NSI::StringArg("shaderfilename", std::string(&m_shaderpath[0])));
	args.Add(new NSI::IntegerArg("mode", triplanar_mode));
	args.Add(new NSI::StringArg("color_texture.meta.colorspace",
								color_space_text.c_str()));
	args.Add(new NSI::StringArg("color_texture", color_texture.c_str()));
	args.Add(new NSI::StringArg("float_texture.meta.colorspace",
								float_color_space_text.c_str()));
	args.Add(new NSI::StringArg("float_texture", float_texture.c_str()));
	args.Add(new NSI::StringArg("height_texture.meta.colorspace",
								height_color_space_text.c_str()));
	args.Add(new NSI::StringArg("height_texture", height_texture.c_str()));
	float triplanar_scale = (float) data->GetFloat(TRANSFORMATION_SCALE);
	args.Add(new NSI::FloatArg("scale", triplanar_scale));
	int triplanar_space = (int) data->GetInt32(TRANSFORMATION_SPACE);
	args.Add(new NSI::IntegerArg("space", triplanar_space));
	float plane_softness = (float) data->GetFloat(PLANE_SOFTNESS);
	args.Add(new NSI::FloatArg("blend_softness", plane_softness));
	float plane_noise_intensity = (float) data->GetFloat(PLANE_NOISE_INTENSITY);
	args.Add(new NSI::FloatArg("blend_noise_intensity", plane_noise_intensity));
	float plane_noise_scale = (float) data->GetFloat(PLANE_NOISE_SCALE);
	args.Add(new NSI::FloatArg("blend_noise_scale", plane_noise_scale));
	bool use_blending = data->GetBool(PLANE_HEIGHT_BASED_BLENDING);
	args.Add(new NSI::IntegerArg("use_height_blending", use_blending));
	float variation_offset = (float) data->GetFloat(PLANE_VARIATION_OFFSET);
	args.Add(new NSI::FloatArg("variation_offset", variation_offset));
	float variation_rotation = (float) data->GetFloat(PLANE_VARIATION_ROTATION);
	args.Add(new NSI::FloatArg("variation_axis_rotation", variation_rotation));
	float variation_scale = (float) data->GetFloat(PLANE_VARIATION_SCALE);
	args.Add(new NSI::FloatArg("variation_scale", variation_scale));
	bool tile_removal = data->GetBool(TILE_REMOVAL_ENABLE);
	args.Add(new NSI::IntegerArg("tile_removal", tile_removal));
	float tile_softness = (float) data->GetFloat(TILE_REMOVAL_SOFTNESS);
	args.Add(new NSI::FloatArg("tile_softness", tile_softness));
	float tile_offset = (float) data->GetFloat(TILE_REMOVAL_OFFSET);
	args.Add(new NSI::FloatArg("tile_offset", tile_offset));
	float tile_rotation = (float) data->GetFloat(TILE_REMOVAL_ROTATION);
	args.Add(new NSI::FloatArg("tile_rotation", tile_rotation));
	float tile_scale = (float) data->GetFloat(TILE_REMOVAL_SCALE);
	args.Add(new NSI::FloatArg("tile_scale", tile_scale));
	ctx.SetAttribute(m_handle, args);
}

void Delight_Triplanar::SampleAttributes(DL_SampleInfo* info,
		const char* Handle,
		BaseList2D* C4DNode,
		BaseDocument* doc,
		DL_SceneParser* parser) {}
