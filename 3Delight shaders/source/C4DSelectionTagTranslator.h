#pragma once

#include "DL_API.h"
#include "c4d.h"

class C4DSelectionTagTranslator : public DL_TranslatorPlugin
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
};
