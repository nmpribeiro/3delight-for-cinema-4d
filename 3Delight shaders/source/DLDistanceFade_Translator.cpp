#include "DLDistanceFade_Translator.h"
#include "dl_distance_fade.h"
#include "3DelightEnvironment.h"

Delight_DistanceFade::Delight_DistanceFade()
{
	is_3delight_shader = true;
	const char *delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath = delightpath + (std::string)"/osl" + (std::string)"/dlDistanceFade.oso";
	if (m_shaderpath.c_str() && m_shaderpath[0])
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);

	m_ids_to_names[MINIMUM_DISTANCE] = std::make_pair("", "minDistance");
	m_ids_to_names[MINIMUM_DISTANCE_SHADER] = std::make_pair("outColor[0]", "minDistance");

	m_ids_to_names[MAXIMUM_DISTANCE] = std::make_pair("", "maxDistance");
	m_ids_to_names[MAXIMUM_DISTANCE_SHADER] = std::make_pair("outColor[0]", "maxDistance");

	m_ids_to_names[FOLLOW_CAM_FOV] = std::make_pair("", "follow_camera_fov");
	m_ids_to_names[FOLLOW_CAM_FOV_SHADER] = std::make_pair("outColor[0]", "follow_camera_fov");

	m_ids_to_names[FADE_CURVE] = std::make_pair("", "fadeCurve");
}

