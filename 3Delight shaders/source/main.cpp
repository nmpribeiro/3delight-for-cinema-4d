#include "main.h"

/*
		This function is used to dynamically change the structure of Create
   Material UI. We create a SubTitle with the name 3Delight when create material
   is pressed. Then under this Subtitle we put all our command plugins as below.
*/
void CreateMaterialUI(BaseContainer* bc, int& created)
{
	if (!bc) {
		return;
	}

	BrowseContainer browse(bc);
	Int32 id = 0;
	GeData* dat = nullptr;
	int index = 0;

	while (browse.GetNext(&id, &dat)) {
		// Should find a more stable way for manually creating and placing this
		// menu of commands(not materials) on materials manager menu.
		if (id == MENURESOURCE_SUBMENU) {
			CreateMaterialUI(dat->GetContainer(), created);

			if (index++ == TRUE && ++created == TRUE) {
				BaseContainer sc;
				sc.InsData(MENURESOURCE_SUBTITLE, String("3Delight"));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_PRINCIPLED_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_GLASS_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_HAIRANDFUR_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_METAL_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_SKIN_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_SUBSTANCE_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_CARPAINT_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_THIN_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_TOON_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_LAYERED_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_RANDOM_MATERIAL_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_STANDARD_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_TOONGLASS_COMMAND));
				sc.InsData(MENURESOURCE_COMMAND,
					"PLUGIN_CMD_" + String::IntToString(DL_SHADOWMATTE_COMMAND));
				bc->InsDataAfter(MENURESOURCE_STRING, sc, dat);
			}
		}
	}
}

Bool PluginStart(void)
{
	// 3Delight Materials Object
	if (!Register_DlPrincipled_Object()) {
		return FALSE;
	}

	if (!Register_DlGlass_Object()) {
		return FALSE;
	}

	if (!Register_HairAndFur_Object()) {
		return FALSE;
	}

	if (!Register_Metal_Object()) {
		return FALSE;
	}

	if (!Register_Skin_Object()) {
		return FALSE;
	}

	if (!Register_Substance_Object()) {
		return FALSE;
	}

	if (!Register_CarPaint_Object()) {
		return FALSE;
	}

	if (!Register_Thin_Object()) {
		return FALSE;
	}

	if (!Register_Toon_Object()) {
		return FALSE;
	}

	if (!Register_ToonGlass_Object()) {
		return FALSE;
	}

	if (!Register_Layered_Object()) {
		return FALSE;
	}

	if (!RegisterDLAtmosphere()) {
		return FALSE;
	}

	if (!Register_RnadomMaterial_Object()) {
		return FALSE;
	}

	if (!Register_Standard_Object()) {
		return FALSE;
	}

	if (!Register_ShadowMatte_Object()) {
		return FALSE;
	}

	// 3Delight Materials
	if (!RegisterDLPrincipled()) {
		return FALSE;
	}

	if (!RegisterDLGlass()) {
		return FALSE;
	}

	if (!RegisterDLSkin()) {
		return FALSE;
	}

	if (!RegisterDLHairAndFur()) {
		return FALSE;
	}

	if (!RegisterDLMetal()) {
		return FALSE;
	}

	if (!RegisterDLSubstance()) {
		return FALSE;
	}

	if (!RegisterDLCarPaint()) {
		return FALSE;
	}

	if (!RegisterDLThin()) {
		return FALSE;
	}

	if (!RegisterDLToon()) {
		return FALSE;
	}

	if (!RegisterDLToonGlass()) {
		return FALSE;
	}

	if (!RegisterDLLayered()) {
		return FALSE;
	}

	if (!RegisterDLRandomMaterial()) {
		return FALSE;
	}

	if (!RegisterDLStandard()) {
		return FALSE;
	}

	if (!RegisterDLShadowMatte()) {
		return FALSE;
	}

	// 3Delight Shaders
	if (!RegisterEnvironmentLight()) {
		return FALSE;
	}

	if (!RegisterSkyTexture()) {
		return FALSE;
	}

	if (!RegisterFlakesTexture()) {
		return FALSE;
	}

	if (!RegisterWorleyNoiseTexture()) {
		return FALSE;
	}

	if (!RegisterColorBlendTexture()) {
		return FALSE;
	}

	if (!RegisterColorCorrectionTexture()) {
		return FALSE;
	}

	if (!RegisterColorVariationTexture()) {
		return FALSE;
	}

	if (!RegisterFacingRatioTexture()) {
		return FALSE;
	}

	if (!RegisterOpenVDB()) {
		return FALSE;
	}

	if (!RegisterRandomColorTexture()) {
		return FALSE;
	}

	if (!RegisterRampTexture()) {
		return FALSE;
	}

	if (!RegisterSolidRampTexture()) {
		return FALSE;
	}

	if (!RegisterBoxNoiseTexture()) {
		return FALSE;
	}

	if (!RegisterTriplanarTexture()) {
		return FALSE;
	}

	if (!RegisterDLPrimvar()) {
		return FALSE;
	}

	if (!RegisterDelightTexture()) {
		return FALSE;
	}

	if (!ConvertC4DMaterial()) {
		return FALSE;
	}

	if (!RegisterAOVs()) {
		return FALSE;
	}

	if (!RegisterAovColor()) {
		return FALSE;
	}

	if (!RegisterAovGroup()) {
		return FALSE;
	}

	if (!RegisterDistanceFadeTexture()) {
		return FALSE;
	}

	// Load custom material preview scene for use by 3Delight materials
	String preview_scene_name("DL_Sphere");
	Filename pluginpath = GeGetPluginPath();
	AddUserPreviewScene(pluginpath.GetString() + "/res/Scenes/DL_Sphere.c4d",
		-1,
		&preview_scene_name);
	return true;
}

void PluginEnd(void) {}

Bool PluginMessage(Int32 id, void* data)
{
	switch (id) {
	case C4DPL_INIT_SYS:
		if (!g_resource.Init()) {
			return FALSE; // don't start plugin without resource
		}

		return TRUE;
		break;

	case DL_LOAD_PLUGINS: {
		DL_PluginManager* pm = (DL_PluginManager*)data;
		pm->RegisterHook(AllocateHook<ShaderSettingsHook>);
		/*
							On the translator we have only allocated the Materials
			   and Shaders that we support in our plugin. There will be others to
			   add as we are working on it.
					*/
					// UVCoord
		pm->RegisterHook(AllocateHook<DelightUVCoord>);
		// C4D Material
		pm->RegisterTranslator(Mmaterial,
			AllocateTranslator<NSI_Export_Material>);
		// 3Delight Materials
		pm->RegisterTranslator(DL_PRINCIPLED,
			AllocateTranslator<Delight_Principled>);
		pm->RegisterTranslator(DL_GLASS, AllocateTranslator<Delight_Glass>);
		pm->RegisterTranslator(DL_SKIN, AllocateTranslator<Delight_Skin>);
		pm->RegisterTranslator(DL_HAIRANDFUR,
			AllocateTranslator<Delight_HairAndFur>);
		pm->RegisterTranslator(DL_METAL, AllocateTranslator<Delight_Metal>);
		pm->RegisterTranslator(DL_SUBSTANCE,
			AllocateTranslator<Delight_Substance>);
		pm->RegisterTranslator(DL_CARPAINT, AllocateTranslator<Delight_CarPaint>);
		pm->RegisterTranslator(DL_THIN, AllocateTranslator<Delight_Thin>);
		pm->RegisterTranslator(DL_ATMOSPHERE_MAT,
			AllocateTranslator<Atmosphere_Translator>);
		pm->RegisterTranslator(DL_TOON, AllocateTranslator<Delight_Toon>);
		pm->RegisterTranslator(DL_TOON_GLASS, AllocateTranslator<Delight_ToonGlass>);
		pm->RegisterTranslator(DL_LAYERED, AllocateTranslator<Delight_Layered>);
		pm->RegisterTranslator(DL_RANDOM_MATERIAL,
			AllocateTranslator<Delight_RandomMaterial>);
		pm->RegisterTranslator(DL_STANDARD, AllocateTranslator<Delight_Standard>);
		pm->RegisterTranslator(DL_SHADOW_MATTE, AllocateTranslator<Delight_ShadowMatte>);
		// C4D Shaders
		// pm->RegisterTranslator(Osky,
		// AllocateTranslator<EnvironmentLightTranslator>);
		pm->RegisterTranslator(Xcheckerboard,
			AllocateTranslator<NSI_Export_Shader>);
		pm->RegisterTranslator(Xbitmap, AllocateTranslator<BitmapTranslator>);
		pm->RegisterTranslator(Ttexture,
			AllocateTranslator<TextureTagTranslator>);
		// 3Delight Shaders
		pm->RegisterTranslator(DL_SKY, AllocateTranslator<Delight_Sky>);
		pm->RegisterTranslator(DL_WorleyNoise,
			AllocateTranslator<Delight_WorleyNoise>);
		pm->RegisterTranslator(DL_Flakes, AllocateTranslator<Delight_Flakes>);
		pm->RegisterTranslator(ID_ENVIRONMENTLIGHT,
			AllocateTranslator<EnvironmentLightTranslator>,
			true);
		pm->RegisterTranslator(DL_COLORBLEND,
			AllocateTranslator<Delight_ColorBlend>);
		pm->RegisterTranslator(DL_COLORCORRECTION,
			AllocateTranslator<Delight_ColorCorrection>);
		pm->RegisterTranslator(DL_COLORVARIATION,
			AllocateTranslator<Delight_ColorVariation>);
		pm->RegisterTranslator(DL_FACINGRATIO,
			AllocateTranslator<Delight_FacingRatio>);
		pm->RegisterTranslator(DL_RANDOMCOLOR,
			AllocateTranslator<Delight_RandomColor>);
		pm->RegisterTranslator(DL_OPENVDB,
			AllocateTranslator<Delight_OpenVDBTranslator>);
		pm->RegisterTranslator(DL_RAMP, AllocateTranslator<Delight_Ramp>);
		pm->RegisterTranslator(DL_SOLID_RAMP,
			AllocateTranslator<Delight_SolidRamp>);
		pm->RegisterTranslator(DL_BOX_NOISE,
			AllocateTranslator<Delight_Box_Noise>);
		pm->RegisterTranslator(DL_TRIPLANAR,
			AllocateTranslator<Delight_Triplanar>);
		pm->RegisterTranslator(DL_PRIMVAR,
			AllocateTranslator<DL_PrimvarTranslator>);
		pm->RegisterTranslator(DL_TEXTURE, AllocateTranslator<Delight_Texture>);
		// C4D Tags
		pm->RegisterTranslator(Tpolygonselection,
			AllocateTranslator<C4DSelectionTagTranslator>);
		break;
	}

	case C4DPL_BUILDMENU: {
		BaseContainer* bc = GetMenuResource("M_MATERIAL_MANAGER"_s);

		if (!bc) {
			return FALSE;
		}

		int created = 0;
		CreateMaterialUI(bc, created);
		break;
	}
	}

	return FALSE;
}
