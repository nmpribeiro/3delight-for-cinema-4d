#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_shadow_matte.h"

class DL_ShadowMatte : public MaterialData
{
	INSTANCEOF(DL_ShadowMatte, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
		const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
		Description* description,
		DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_ShadowMatte);
	}
};

Bool DL_ShadowMatte::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*)node);
	const Float step = 360;
	BaseContainer* data = ((BaseMaterial*)node)->GetDataInstance();
	Vector shadow_color = HSVToRGB(Vector(0,0,0));
	data->SetVector(SHADOW_MATTE_COLOR, shadow_color);
	data->SetFloat(SHADOW_MATTE_OPACITY, 1.0);
	data->SetVector(SHADOW_DIFFUSE_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(SHADOW_DIFFUSE_INTENSITY, 0.8);
	data->SetFloat(SHADOW_DIFFUSE_ROUGHNESS, 0.3);

	return true;
}

Bool DL_ShadowMatte::GetDDescription(GeListNode* node,
	Description* description,
	DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_SHADOW_MATTE);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*)node)->GetDataInstance();
	HideAndShowTextures(SHADOW_MATTE_COLOR_GROUP_PARAM, SHADOW_MATTE_COLOR, 
		SHADOW_MATTE_COLOR_SHADER, SHADOW_MATTE_COLOR_SHADER_TEMP, node, description, dldata);	
	HideAndShowTextures(SHADOW_MATTE_OPACITY_GROUP_PARAM, SHADOW_MATTE_OPACITY,
		SHADOW_MATTE_OPACITY_SHADER, SHADOW_MATTE_OPACITY_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SHADOW_DIFFUSE_COLOR_GROUP_PARAM, SHADOW_DIFFUSE_COLOR,
		SHADOW_DIFFUSE_COLOR_SHADER, SHADOW_DIFFUSE_COLOR_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SHADOW_DIFFUSE_INTENSITY_GROUP_PARAM, SHADOW_DIFFUSE_INTENSITY,
		SHADOW_DIFFUSE_INTENSITY_SHADER, SHADOW_DIFFUSE_INTENSITY_SHADER_TEMP, node, description, dldata);
	HideAndShowTextures(SHADOW_DIFFUSE_ROUGHNESS_GROUP_PARAM, SHADOW_DIFFUSE_ROUGHNESS,
		SHADOW_DIFFUSE_ROUGHNESS_SHADER, SHADOW_DIFFUSE_ROUGHNESS_SHADER_TEMP, node, description, dldata);
	
	return TRUE;
}

Bool DL_ShadowMatte::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*)node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*)data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_SHADOW_MATTE_COLOR:
			FillPopupMenu(dldata, dp, SHADOW_MATTE_COLOR_GROUP_PARAM);
			break;

		case POPUP_SHADOW_MATTE_OPACITY:
			FillPopupMenu(dldata, dp, SHADOW_MATTE_OPACITY_GROUP_PARAM);
			break;

		case POPUP_SHADOW_DIFFUSE_COLOR:
			FillPopupMenu(dldata, dp, SHADOW_DIFFUSE_COLOR_GROUP_PARAM);
			break;

		case POPUP_SHADOW_DIFFUSE_INTENSITY:
			FillPopupMenu(dldata, dp, SHADOW_DIFFUSE_INTENSITY_GROUP_PARAM);
			break;

		case POPUP_SHADOW_DIFFUSE_ROUGHNESS:
			FillPopupMenu(dldata, dp, SHADOW_DIFFUSE_ROUGHNESS_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_ShadowMatte::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_ShadowMatte::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLShadowMatte(void)
{
	return RegisterMaterialPlugin(
		DL_SHADOW_MATTE, "Shadow Matte"_s, PLUGINFLAG_HIDE, DL_ShadowMatte::Alloc, "dl_shadow_matte"_s, 0);
}
