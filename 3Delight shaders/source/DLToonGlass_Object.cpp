#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_ToonGlass_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_ToonGlass_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_TOON_GLASS, doc);
	return true;
}

Bool Register_ToonGlass_Object(void)
{
	DL_ToonGlass_command* new_glass_toon = NewObjClear(DL_ToonGlass_command);
	RegisterCommandPlugin(DL_TOONGLASS_COMMAND,
						  "Toon Glass"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlToonGlass_200.png"_s),
						  String("Assign new Glass Toon"_s),
						  NewObjClear(DL_ToonGlass_command));

	if (RegisterCommandPlugin(DL_TOON_GLASS_SHELF_COMMAND,
							  "Toon Glass"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlToonGlass_200.png"_s),
							  String("Assign new Glass Toon Material"),
							  new_glass_toon)) {
		new_glass_toon->shelf_used = 1;
	}

	return true;
}
