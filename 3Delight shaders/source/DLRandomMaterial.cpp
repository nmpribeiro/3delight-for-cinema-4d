#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_random_material.h"

class DL_RandomMaterial : public MaterialData
{
	INSTANCEOF(DL_RandomMaterial, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc(void)
	{
		return NewObjClear(DL_RandomMaterial);
	}
};

Bool DL_RandomMaterial::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	const Float step = 360;
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetFloat(INPUT_IMPORTANCE1, 1.0);
	data->SetFloat(INPUT_IMPORTANCE2, 1.0);
	data->SetFloat(INPUT_IMPORTANCE3, 1.0);
	data->SetFloat(INPUT_IMPORTANCE4, 1.0);
	data->SetFloat(INPUT_IMPORTANCE5, 1.0);
	return true;
}

Bool DL_RandomMaterial::GetDDescription(GeListNode* node,
										Description* description,
										DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_RANDOM_MATERIAL);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(INPUT_IMPORTANCE1_GROUP_PARAM,
						INPUT_IMPORTANCE1,
						INPUT_IMPORTANCE1_SHADER,
						INPUT_IMPORTANCE1_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE2_GROUP_PARAM,
						INPUT_IMPORTANCE2,
						INPUT_IMPORTANCE2_SHADER,
						INPUT_IMPORTANCE2_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE3_GROUP_PARAM,
						INPUT_IMPORTANCE3,
						INPUT_IMPORTANCE3_SHADER,
						INPUT_IMPORTANCE3_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE4_GROUP_PARAM,
						INPUT_IMPORTANCE4,
						INPUT_IMPORTANCE4_SHADER,
						INPUT_IMPORTANCE4_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(INPUT_IMPORTANCE5_GROUP_PARAM,
						INPUT_IMPORTANCE5,
						INPUT_IMPORTANCE5_SHADER,
						INPUT_IMPORTANCE5_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_RandomMaterial::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_INPUT_IMPORTANCE1:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE1_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE2:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE2_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE3:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE3_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE4:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE4_GROUP_PARAM);
			break;

		case POPUP_INPUT_IMPORTANCE5:
			FillPopupMenu(dldata, dp, INPUT_IMPORTANCE5_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_RandomMaterial::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_RandomMaterial::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLRandomMaterial(void)
{
	return RegisterMaterialPlugin(DL_RANDOM_MATERIAL,
								  "Random Material"_s,
								  PLUGINFLAG_HIDE,
								  DL_RandomMaterial::Alloc,
								  "dl_random_material"_s,
								  0);
}
