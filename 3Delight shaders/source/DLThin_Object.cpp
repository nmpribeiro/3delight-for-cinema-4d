#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_Thin_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_Thin_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_THIN, doc);
	return true;
}

Bool Register_Thin_Object(void)
{
	DL_Thin_command* new_thin = NewObjClear(DL_Thin_command);
	RegisterCommandPlugin(DL_THIN_COMMAND,
						  "Thin"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("Thin_Material.png"_s),
						  String("Assign new Thin"_s),
						  NewObjClear(DL_Thin_command));

	if (RegisterCommandPlugin(DL_THIN_SHELF_COMMAND,
							  "Thin"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("Thin_Material.png"_s),
							  String("Assign new Thin Material"),
							  new_thin)) {
		new_thin->shelf_used = 1;
	}

	return true;
}
