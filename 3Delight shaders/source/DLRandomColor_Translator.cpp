#include "3DelightEnvironment.h"
#include "DLRandomColorTranslator.h"
#include "DL_TypeConversions.h"
#include "DL_Utilities.h"
#include "c4d.h"
#include "dl_random_color.h"
#include "nsi.hpp"

Delight_RandomColor::Delight_RandomColor()
{
	is_3delight_shader = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath = delightpath + (std::string) "/osl" + (std::string) "/dlRandomInputColor.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[INPUT_COLOR1] = std::make_pair("", "color_1");
	m_ids_to_names[INPUT_COLOR1_SHADER] = std::make_pair("outColor", "color_1");
	m_ids_to_names[INPUT_IMPORTANCE1] = std::make_pair("", "importance_1");
	m_ids_to_names[INPUT_IMPORTANCE1_SHADER] =
		std::make_pair("outColor[0]", "importance_1");
	m_ids_to_names[INPUT_COLOR2] = std::make_pair("", "color_2");
	m_ids_to_names[INPUT_COLOR2_SHADER] = std::make_pair("outColor", "color_2");
	m_ids_to_names[INPUT_IMPORTANCE2] = std::make_pair("", "importance_2");
	m_ids_to_names[INPUT_IMPORTANCE2_SHADER] =
		std::make_pair("outColor[0]", "importance_2");
	m_ids_to_names[INPUT_COLOR3] = std::make_pair("", "color_3");
	m_ids_to_names[INPUT_COLOR3_SHADER] = std::make_pair("outColor", "color_3");
	m_ids_to_names[INPUT_IMPORTANCE3] = std::make_pair("", "importance_3");
	m_ids_to_names[INPUT_IMPORTANCE3_SHADER] =
		std::make_pair("outColor[0]", "importance_3");
	m_ids_to_names[INPUT_COLOR4] = std::make_pair("", "color_4");
	m_ids_to_names[INPUT_COLOR4_SHADER] = std::make_pair("outColor", "color_4");
	m_ids_to_names[INPUT_IMPORTANCE4] = std::make_pair("", "importance_4");
	m_ids_to_names[INPUT_IMPORTANCE4_SHADER] =
		std::make_pair("outColor[0]", "importance_4");
	m_ids_to_names[INPUT_COLOR5] = std::make_pair("", "color_5");
	m_ids_to_names[INPUT_COLOR5_SHADER] = std::make_pair("outColor", "color_5");
	m_ids_to_names[INPUT_IMPORTANCE5] = std::make_pair("", "importance_5");
	m_ids_to_names[INPUT_IMPORTANCE5_SHADER] =
		std::make_pair("outColor[0]", "importance_5");
}
