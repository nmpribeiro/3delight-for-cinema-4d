#include "DLWorleyNoise_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_worley_noise.h"

Delight_WorleyNoise::Delight_WorleyNoise()
{
	is_3delight_shader = true;
	has_uv_connection = false;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlWorleyNoise.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[WORLEY_NOISE_OUTPUT_TYPE] = std::make_pair("", "output_type");
	m_ids_to_names[WORLEY_NOISE_DISTANCE_TYPE] =
		std::make_pair("", "distance_type");
	m_ids_to_names[WORLEY_NOISE_MINKOWSKI_NUMBER] =
		std::make_pair("", "minkowski_n");
	m_ids_to_names[WORLEY_NOISE_MINKOWSKI_NUMBER_SHADER] =
		std::make_pair("outColor[0]", "minkowski_n");
	m_ids_to_names[WORLEY_NOISE_SCALE] = std::make_pair("", "scale");
	m_ids_to_names[WORLEY_NOISE_SCALE_SHADER] =
		std::make_pair("outColor[0]", "scale");
	m_ids_to_names[WORLEY_NOISE_DENSITY] = std::make_pair("", "density");
	m_ids_to_names[WORLEY_NOISE_DENSITY_SHADER] =
		std::make_pair("outColor[0]", "density");
	m_ids_to_names[WORLEY_NOISE_RANDOM_POSITION] =
		std::make_pair("", "random_pos");
	m_ids_to_names[WORLEY_NOISE_RANDOM_POSITION_SHADER] =
		std::make_pair("outColor[0]", "random_pos");
	m_ids_to_names[WORLEY_NOISE_TIME] = std::make_pair("", "i_time");
	m_ids_to_names[WORLEY_NOISE_TIME_SHADER] =
		std::make_pair("outColor[0]", "i_time");
	m_ids_to_names[WORLEY_NOISE_SPACE] = std::make_pair("", "space");
	m_ids_to_names[WORLEY_NOISE_BLEND_MODE] =
		std::make_pair("", "color_blend_mode");
	m_ids_to_names[WORLEY_NOISE_BACKGROUND_COLOR] =
		std::make_pair("", "color_bg");
	m_ids_to_names[WORLEY_NOISE_BACKGROUND_COLOR_SHADER] =
		std::make_pair("outColor", "color_bg");
	m_ids_to_names[WORLEY_NOISE_DISTORTION] = std::make_pair("", "distortion");
	m_ids_to_names[WORLEY_NOISE_DISTORTION_SHADER] =
		std::make_pair("outColor", "distortion");
	m_ids_to_names[WORLEY_NOISE_DISTORTION_INTENSITY] =
		std::make_pair("", "distortion_intensity");
	m_ids_to_names[WORLEY_NOISE_DISTORTION_INTENSITY_SHADER] =
		std::make_pair("outColor[0]", "distortion_intensity");
	m_ids_to_names[WORLEY_NOISE_BORDERS_THICKNESS] =
		std::make_pair("", "border_thickness");
	m_ids_to_names[WORLEY_NOISE_BORDERS_THICKNESS_SHADER] =
		std::make_pair("outColor[0]", "border_thickness");
	m_ids_to_names[WORLEY_NOISE_BORDERS_SMOOTHNESS] =
		std::make_pair("", "border_smoothness");
	m_ids_to_names[WORLEY_NOISE_BORDERS_SMOOTHNESS_SHADER] =
		std::make_pair("outColor[0]", "border_smoothness");
	m_ids_to_names[WORLEY_NOISE_LAYERS] = std::make_pair("", "layers");
	m_ids_to_names[WORLEY_NOISE_PERSISTENCE] =
		std::make_pair("", "layer_persistence");
	m_ids_to_names[WORLEY_NOISE_PERSISTENCE_SHADER] =
		std::make_pair("outColor[0]", "layer_persistence");
	m_ids_to_names[WORLEY_NOISE_LAYERS_SCALE] = std::make_pair("", "layer_scale");
	m_ids_to_names[WORLEY_NOISE_LAYERS_SCALE_SHADER] =
		std::make_pair("outColor[0]", "layer_scale");
	m_ids_to_names[WORLEY_NOISE_LAYERS_DENSITY] =
		std::make_pair("", "layer_density");
	m_ids_to_names[WORLEY_NOISE_LAYERS_DENSITY_SHADER] =
		std::make_pair("outColor[0]", "layer_density");
	m_ids_to_names[WORLEY_NOISE_ADJUST_AMPLITUDE] =
		std::make_pair("", "amplitude");
	m_ids_to_names[WORLEY_NOISE_ADJUST_AMPLITUDE_SHADER] =
		std::make_pair("outColor[0]", "amplitude");
	m_ids_to_names[WORLEY_NOISE_ADJUST_CONTRAST] = std::make_pair("", "contrast");
	m_ids_to_names[WORLEY_NOISE_ADJUST_CONTRAST_SHADER] =
		std::make_pair("outColor[0]", "contrast");
	m_ids_to_names[WORLEY_NOISE_ADJUST_INVERT] = std::make_pair("", "invert");
	m_ids_to_names[NOISE_COLORS_INSIDE_CELL] =
		std::make_pair("", "color_in_cell");
	m_ids_to_names[NOISE_COLORS_ACROSS_CELL] =
		std::make_pair("", "color_across_cell");
}
