#include <DL_Utilities.h>
#include "IDs.h"
#include "c4d.h"

class DL_CarPaint_command : public CommandData
{
public:
	int shelf_used = 0;
	virtual Bool Execute(BaseDocument* doc, GeDialog* parentManager);
};

Bool DL_CarPaint_command::Execute(BaseDocument* doc, GeDialog* parentManager)
{
	CreateMaterial(shelf_used, DL_CARPAINT, doc);
	return true;
}

Bool Register_CarPaint_Object(void)
{
	DL_CarPaint_command* new_car_paint = NewObjClear(DL_CarPaint_command);
	RegisterCommandPlugin(DL_CARPAINT_COMMAND,
						  "Car Paint"_s,
						  PLUGINFLAG_HIDEPLUGINMENU,
						  AutoBitmap("shelf_dlCarMaterial_200.png"_s),
						  String("Assign new Car Paint"_s),
						  NewObjClear(DL_CarPaint_command));

	if (RegisterCommandPlugin(DL_CARPAINT_SHELF_COMMAND,
							  "Car Paint"_s,
							  PLUGINFLAG_HIDEPLUGINMENU,
							  AutoBitmap("shelf_dlCarMaterial_200.png"_s),
							  String("Assign new Car Paint Material"_s),
							  new_car_paint)) {
		new_car_paint->shelf_used = 1;
	}

	return true;
}
