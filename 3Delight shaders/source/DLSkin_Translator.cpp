#include "DLSkin_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_skin.h"

Delight_Skin::Delight_Skin()
{
	is_3delight_material = true;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlSkin.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &c_shaderpath[0]);
	}

	m_ids_to_names[SKIN_COLOR] = std::make_pair("", "skin_color");
	m_ids_to_names[SKIN_COLOR_SHADER] = std::make_pair("outColor", "skin_color");
	m_ids_to_names[SKIN_ROUGHNESS] = std::make_pair("", "roughness");
	m_ids_to_names[SKIN_ROUGHNESS_SHADER] =
		std::make_pair("outColor[0]", "roughness");
	m_ids_to_names[SKIN_SPECULAR_LEVEL] = std::make_pair("", "specular_level");
	m_ids_to_names[SKIN_SPECULAR_LEVEL_SHADER] =
		std::make_pair("outColor[0]", "specular_level");
	m_ids_to_names[SUBSURFACE_COLOR] = std::make_pair("", "sss_color");
	m_ids_to_names[SUBSURFACE_COLOR_SHADER] =
		std::make_pair("outColor", "sss_color");
	m_ids_to_names[SUBSURFACE_SCALE] = std::make_pair("", "sss_scale");
	m_ids_to_names[SUBSURFACE_SCALE_SHADER] =
		std::make_pair("outColor[0]", "sss_scale");
	m_ids_to_names[SUBSURFACE_IOR] = std::make_pair("", "sss_ior");
	m_ids_to_names[SUBSURFACE_IOR_SHADER] = std::make_pair("outColor[0]", "sss_ior");
	m_ids_to_names[SUBSURFACE_ANISOTROPY] = std::make_pair("", "sss_anisotropy");
	m_ids_to_names[SUBSURFACE_ANISOTROPY_SHADER] = std::make_pair("outColor[0]", "sss_anisotropy");
	m_ids_to_names[BUMP_TYPE] = std::make_pair("", "disp_normal_bump_type");
	m_ids_to_names[BUMP_VALUE] =
		std::make_pair("outColor", "disp_normal_bump_value");
	m_ids_to_names[BUMP_INTENSITY] =
		std::make_pair("", "disp_normal_bump_intensity");
	m_ids_to_names[DL_AOV_GROUP] = std::make_pair("outColor", "aovGroup");
}
