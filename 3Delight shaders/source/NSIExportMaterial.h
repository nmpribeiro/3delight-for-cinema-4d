#pragma once

#include <map>
#include "DL_TranslatorPlugin.h"

class NSI_Export_Material : public DL_TranslatorPlugin
{
public:
	virtual void CreateNSINodes(const char* Handle,
								const char* ParentTransformHandle,
								BaseList2D* C4DNode,
								BaseDocument* doc,
								DL_SceneParser* parser);
	virtual void ConnectNSINodes(const char* Handle,
								 const char* ParentTransformHandle,
								 BaseList2D* C4DNode,
								 BaseDocument* doc,
								 DL_SceneParser* parser);
	virtual void SampleAttributes(DL_SampleInfo* info,
								  const char* Handle,
								  BaseList2D* C4DNode,
								  BaseDocument* doc,
								  DL_SceneParser* parser);

	std::map<int, std::pair<std::string, std::string>> m_ids_to_names;
	bool is_3delight_material = false;

protected:
	std::string c_shaderpath;
	bool has_uv_connection = true;
};
