#include "DL_Matpreview.h"
#include "IDs.h"
#include "TextureUI_Functions.h"
#include "c4d.h"
#include "c4d_symbols.h"
#include "dl_toon_glass.h"

class DL_ToonGlass : public MaterialData
{
	INSTANCEOF(DL_ToonGlass, MaterialData)

private:
	Vector color;

public:
	virtual Bool Init(GeListNode* node);
	virtual INITRENDERRESULT InitRender(BaseMaterial* mat,
										const InitRenderStruct& irs);
	virtual Bool GetDDescription(GeListNode* node,
								 Description* description,
								 DESCFLAGS_DESC& flags);
	virtual Bool GetDEnabling(GeListNode* node,
							  const DescID& id,
							  const GeData& t_data,
							  DESCFLAGS_ENABLE flags,
							  const BaseContainer* itemdesc);
	virtual Bool Message(GeListNode* node, Int32 type, void* data);
	virtual void CalcSurface(BaseMaterial* mat, VolumeData* vd);
	static NodeData* Alloc()
	{
		return NewObjClear(DL_ToonGlass);
	}
};

Bool DL_ToonGlass::Init(GeListNode* node)
{
	// Set default matpreview scene
	DL_SetDefaultMatpreview((BaseMaterial*) node);
	BaseContainer* data = ((BaseMaterial*) node)->GetDataInstance();
	data->SetVector(REFRACTION_COLOR, HSVToRGB(Vector(0, 0, 1)));
	data->SetFloat(REFRACTION_IOR, 1.3);

	data->SetBool(OUTLINES_ENABLE, FALSE);
	data->SetVector(OUTLINES_COLOR, HSVToRGB(Vector(0, 0, 0.5)));
	data->SetFloat(OUTLINES_OPACITY, 1.0);
	data->SetFloat(OUTLINES_WIDTH, 1);
	data->SetInt32(OUTLINES_PRIORITY, 0);
	return true;
}

Bool DL_ToonGlass::GetDEnabling(GeListNode* node,
						   const DescID& id,
						   const GeData& t_data,
						   DESCFLAGS_ENABLE flags,
						   const BaseContainer* itemdesc)
{
	if (!node) {
		return false;
	}

	return true;
}

Bool DL_ToonGlass::GetDDescription(GeListNode* node,
							  Description* description,
							  DESCFLAGS_DESC& flags)
{
	description->LoadDescription(DL_TOON_GLASS);
	flags |= DESCFLAGS_DESC::LOADED;
	BaseContainer* dldata = ((BaseObject*) node)->GetDataInstance();
	HideAndShowTextures(REFRACTION_COLOR_GROUP_PARAM,
						REFRACTION_COLOR,
						REFRACTION_COLOR_SHADER,
						REFRACTION_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(REFRACTION_IOR_GROUP_PARAM,
						REFRACTION_IOR,
						REFRACTION_IOR_SHADER,
						REFRACTION_IOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OUTLINES_COLOR_GROUP_PARAM,
						OUTLINES_COLOR,
						OUTLINES_COLOR_SHADER,
						OUTLINES_COLOR_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OUTLINES_OPACITY_GROUP_PARAM,
						OUTLINES_OPACITY,
						OUTLINES_OPACITY_SHADER,
						OUTLINES_OPACITY_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OUTLINES_WIDTH_GROUP_PARAM,
						OUTLINES_WIDTH,
						OUTLINES_WIDTH_SHADER,
						OUTLINES_WIDTH_SHADER_TEMP,
						node,
						description,
						dldata);
	HideAndShowTextures(OUTLINES_PRIORITY_GROUP_PARAM,
						OUTLINES_PRIORITY,
						OUTLINES_PRIORITY_SHADER,
						OUTLINES_PRIORITY_SHADER_TEMP,
						node,
						description,
						dldata);
	return TRUE;
}

Bool DL_ToonGlass::Message(GeListNode* node, Int32 type, void* data)
{
	if (!node) {
		return false;
	}

	BaseContainer* dldata = ((BaseMaterial*) node)->GetDataInstance();

	if (type == MSG_DESCRIPTION_POPUP) {
		DescriptionPopup* dp = (DescriptionPopup*) data;
		int clicked_button_id = dp->_descId[0].id;

		switch (clicked_button_id) {
		case POPUP_REFRACTION_COLOR:
			FillPopupMenu(dldata, dp, REFRACTION_COLOR_GROUP_PARAM);
			break;

		case POPUP_REFRACTION_IOR:
			FillPopupMenu(dldata, dp, REFRACTION_IOR_GROUP_PARAM);
			break;

		case POPUP_OUTLINES_COLOR:
			FillPopupMenu(dldata, dp, OUTLINES_COLOR_GROUP_PARAM);
			break;

		case POPUP_OUTLINES_OPACITY:
			FillPopupMenu(dldata, dp, OUTLINES_OPACITY_GROUP_PARAM);
			break;

		case POPUP_OUTLINES_WIDTH:
			FillPopupMenu(dldata, dp, OUTLINES_WIDTH_GROUP_PARAM);
			break;

		case POPUP_OUTLINES_PRIORITY:
			FillPopupMenu(dldata, dp, OUTLINES_PRIORITY_GROUP_PARAM);
			break;

		default:
			break;
		}
	}

	return true;
}

// Initializes resources for rendering.
INITRENDERRESULT
DL_ToonGlass::InitRender(BaseMaterial* mat, const InitRenderStruct& irs)
{
	return INITRENDERRESULT::OK;
}

void DL_ToonGlass::CalcSurface(BaseMaterial* mat, VolumeData* vd)
{
	Vector diff, spec;
	vd->IlluminanceSimple(&diff, &spec, 0, 0, 0);
	vd->col = 0.8 * diff;
}

Bool RegisterDLToonGlass(void)
{
	return RegisterMaterialPlugin(
			   DL_TOON_GLASS, "Toon Glass"_s, PLUGINFLAG_HIDE, DL_ToonGlass::Alloc, "dl_toon_glass"_s, 0);
}
