#include "DLColorBlend_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_color_blend.h"

Delight_ColorBlend::Delight_ColorBlend()
{
	is_3delight_shader = true;
	has_uv_connection = false;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	m_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlColorBlend.oso";

	if (m_shaderpath.c_str() && m_shaderpath[0]) {
		m_ids_to_names[Shader_Path] = std::make_pair("", &m_shaderpath[0]);
	}

	m_ids_to_names[BLEND_MODE] = std::make_pair("", "blendMode");
	m_ids_to_names[BLEND_FACTOR] = std::make_pair("", "blend");
	m_ids_to_names[BLEND_FACTOR_SHADER] = std::make_pair("outColor[0]", "blend");
	m_ids_to_names[BLEND_FOREGROUND] = std::make_pair("", "fg");
	m_ids_to_names[BLEND_FOREGROUND_SHADER] = std::make_pair("outColor", "fg");
	m_ids_to_names[BLEND_BACKGROUND] = std::make_pair("", "bg");
	m_ids_to_names[BLEND_BACKGROUND_SHADER] = std::make_pair("outColor", "bg");
}
