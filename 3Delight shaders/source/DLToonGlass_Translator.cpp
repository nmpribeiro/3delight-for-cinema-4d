#include "DLToonGlass_Translator.h"
#include "3DelightEnvironment.h"
#include "dl_toon_glass.h"

Delight_ToonGlass::Delight_ToonGlass()
{
	is_3delight_material = true;
	has_uv_connection = false;
	const char* delightpath = DelightEnv::getDelightEnvironment();
	c_shaderpath =
		delightpath + (std::string) "/osl" + (std::string) "/dlToonGlass.oso";

	if (c_shaderpath.c_str() && c_shaderpath[0]) {
		m_ids_to_names[TOON_GLASS_SHADER_PATH] = std::make_pair("", &c_shaderpath[0]);
	}

	//Refraction Group
	m_ids_to_names[REFRACTION_COLOR] =
		std::make_pair("", "i_color");
	m_ids_to_names[REFRACTION_COLOR_SHADER] =
		std::make_pair("outColor", "i_color");
	m_ids_to_names[REFRACTION_IOR] =
		std::make_pair("", "refract_ior");
	m_ids_to_names[REFRACTION_IOR_SHADER] =
		std::make_pair("outColor[0]", "refract_ior");
	
	//Outlines Group
	m_ids_to_names[OUTLINES_ENABLE] =
		std::make_pair("", "fg_outline_objects_enable");
	m_ids_to_names[OUTLINES_COLOR] =
		std::make_pair("", "fg_outline_objects_color");
	m_ids_to_names[OUTLINES_COLOR_SHADER] =
		std::make_pair("outColor", "i_color");
	m_ids_to_names[OUTLINES_OPACITY] =
		std::make_pair("", "fg_outline_objects_opacity");
	m_ids_to_names[OUTLINES_OPACITY_SHADER] =
		std::make_pair("outColor[0]", "fg_outline_objects_opacity");
	m_ids_to_names[OUTLINES_WIDTH] =
		std::make_pair("", "fg_outline_objects_width");
	m_ids_to_names[OUTLINES_WIDTH_SHADER] =
		std::make_pair("outColor[0]", "fg_outline_objects_width");
	m_ids_to_names[OUTLINES_PRIORITY] =
		std::make_pair("", "fg_outline_objects_priority");
	m_ids_to_names[OUTLINES_PRIORITY_SHADER] =
		std::make_pair("outColor[0]", "fg_outline_objects_priority");

	m_ids_to_names[DL_AOV_GROUP] =
		std::make_pair("outColor", "aovGroup");
}
