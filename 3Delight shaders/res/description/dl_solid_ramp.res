CONTAINER dl_solid_ramp
{
	NAME dl_solid_ramp;

	GROUP SHADER_GROUP
	{	
		COLUMNS 2;

		LONG SOLID_RAMP_SHAPE
		{
			SCALE_H;
			CYCLE
			{
				SOLID_RAMP_SHAPE_SPHERE;
				SOLID_RAMP_SHAPE_CUBE;
				SOLID_RAMP_SHAPE_CYLINDER;
				SOLID_RAMP_SHAPE_CONE;
				SOLID_RAMP_SHAPE_LINEAR;
			}
		}
		SEPARATOR{}
		
		GRADIENT COLOR_SOLID_RAMP_GRADIENT {COLOR;}
		SEPARATOR{}
		

		SPLINE OPACITY_SOLID_RAMP_GRADIENT
		{
		  SHOWGRID_H;
		  SHOWGRID_V;
		  EDIT_H;
		  EDIT_V;
		  NO_FLOATING_WINDOW;
		}
		SEPARATOR{}
	
		
		COLOR SOLID_RAMP_BACKGROUND_COLOR{SCALE_H;}
		SHADERLINK SOLID_RAMP_BACKGROUND_COLOR_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_BACKGROUND_COLOR_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_BACKGROUND_COLOR{SCALE_V;}
		
		LONG SOLID_RAMP_REVERSE
		{
			SCALE_H;
			CYCLE
			{
				SOLID_RAMP_REVERSE_NONE;
				SOLID_RAMP_REVERSE_BOTH;
				SOLID_RAMP_REVERSE_COLOR_RAMP;
				SOLID_RAMP_REVERSE_OPACITY_RAMP;
			}
		}
		
		SEPARATOR{}
				
		LONG SOLID_RAMP_OUTSIDE
		{
			SCALE_H;
			CYCLE
			{
				SOLID_RAMP_OUTSIDE_ONLY_BACK;
				SOLID_RAMP_OUTSIDE_REPEAT_RAMP;
			}
		}
		
		SEPARATOR{}
				
		LONG SOLID_RAMP_SPACE
		{
			SCALE_H;
			CYCLE
			{
				SOLID_RAMP_SPACE_WORLD;
				SOLID_RAMP_SPACE_OBJECT;
			}
		}
		
		SEPARATOR{}

		GROUP
		{	
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR SOLID_RAMP_DISTORTION_GROUP{}
			STATICTEXT{}	
		}
		
				
		REAL SOLID_RAMP_DISTORTION_INTENSITY{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_DISTORTION_INTENSITY_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_DISTORTION_INTENSITY_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_DISTORTION_INTENSITY{SCALE_V;}
		
		SHADERLINK SOLID_RAMP_DISTORTION_SHADER{ANIM OFF;}
		SEPARATOR{}

		
		GROUP
		{	
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR SOLID_RAMP_NOISE_TRANSFORM_GROUP{}
			STATICTEXT{}	
		}
		
	
		REAL SOLID_RAMP_NOISE_INTENSITY{SCALE_H;MIN 0; MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_INTENSITY_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_INTENSITY_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_INTENSITY{SCALE_V;}

		REAL SOLID_RAMP_NOISE_SCALE{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_SCALE_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_SCALE_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_SCALE{SCALE_V;}
		
		REAL SOLID_RAMP_NOISE_STEPPING{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_STEPPING_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_STEPPING_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_STEPPING{SCALE_V;}
		
		REAL SOLID_RAMP_NOISE_TIME{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_TIME_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_TIME_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_TIME{SCALE_V;}
		
			
		GROUP
		{	
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR SOLID_RAMP_NOISE_GROUP{}
			STATICTEXT{}	
		}
		
		REAL SOLID_RAMP_NOISE_HUE{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_HUE_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_HUE_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_HUE{SCALE_V;}		
		
		REAL SOLID_RAMP_NOISE_SATURATION{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_SATURATION_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_SATURATION_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_SATURATION{SCALE_V;}		
		
		REAL SOLID_RAMP_NOISE_VALUE{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_VALUE_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_VALUE_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_VALUE{SCALE_V;}		
		
		REAL SOLID_RAMP_NOISE_OPACITY{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_OPACITY_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_OPACITY_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_OPACITY{SCALE_V;}
		
		GROUP
		{	
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR SOLID_RAMP_NOISE_LAYERS_GROUP{}
			STATICTEXT{}	
		}
		
		LONG SOLID_RAMP_NOISE_LAYERS{SCALE_H;MIN 0;MINSLIDER 1;}
		STATICTEXT{}	

		REAL SOLID_RAMP_NOISE_LAYERS_PERSISTENCE{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_LAYERS_PERSISTENCE_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_LAYERS_PERSISTENCE{SCALE_V;}		
		
		REAL SOLID_RAMP_NOISE_LAYERS_SCALE{SCALE_H;MIN 0;MINSLIDER 0; MAXSLIDER 1; STEP 0.01; CUSTOMGUI REALSLIDER;}
		SHADERLINK SOLID_RAMP_NOISE_LAYERS_SCALE_SHADER{ANIM OFF;HIDDEN;}
		SHADERLINK SOLID_RAMP_NOISE_LAYERS_SCALE_SHADER_TEMP{ANIM OFF;HIDDEN;}
		POPUP POPUP_SOLID_RAMP_NOISE_LAYERS_SCALE{SCALE_V;}
				
		GROUP
		{	
			COLUMNS 2;
			STATICTEXT{}
			STATICTEXT{}
			SEPARATOR SOLID_RAMP_ADJUST_GROUP{}
			STATICTEXT{}	
		}
		
		BOOL SOLID_RAMP_ADJUST_INVERT{}
		STATICTEXT{}
		
	}
}

