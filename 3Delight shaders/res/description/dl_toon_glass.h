#pragma once
enum {
	TOON_GLASS_SHADER_PATH = 0,
	TOON_GLASS_PAGE = 100,
	TOON_GLASS_GROUP,

	REFRACTION_PAGE,
	REFRACTION,

	POPUP_REFRACTION_COLOR,
	REFRACTION_COLOR,
	REFRACTION_COLOR_GROUP_PARAM,
	REFRACTION_COLOR_SHADER,
	REFRACTION_COLOR_SHADER_TEMP,

	POPUP_REFRACTION_IOR,
	REFRACTION_IOR,
	REFRACTION_IOR_GROUP_PARAM,
	REFRACTION_IOR_SHADER,
	REFRACTION_IOR_SHADER_TEMP,

	OUTLINES_PAGE,
	OUTLINES,

	OUTLINES_ENABLE,

	POPUP_OUTLINES_COLOR,
	OUTLINES_COLOR,
	OUTLINES_COLOR_GROUP_PARAM,
	OUTLINES_COLOR_SHADER,
	OUTLINES_COLOR_SHADER_TEMP,

	POPUP_OUTLINES_OPACITY,
	OUTLINES_OPACITY,
	OUTLINES_OPACITY_GROUP_PARAM,
	OUTLINES_OPACITY_SHADER,
	OUTLINES_OPACITY_SHADER_TEMP,

	POPUP_OUTLINES_WIDTH,
	OUTLINES_WIDTH,
	OUTLINES_WIDTH_GROUP_PARAM,
	OUTLINES_WIDTH_SHADER,
	OUTLINES_WIDTH_SHADER_TEMP,

	POPUP_OUTLINES_PRIORITY,
	OUTLINES_PRIORITY,
	OUTLINES_PRIORITY_GROUP_PARAM,
	OUTLINES_PRIORITY_SHADER,
	OUTLINES_PRIORITY_SHADER_TEMP,

	//POPUP_OUTLINES_FADE,
	//OUTLINES_FADE,
	//OUTLINES_FADE_GROUP_PARAM,
	//OUTLINES_FADE_SHADER,
	//OUTLINES_FADE_SHADER_TEMP,

	AOV_PAGE,
	DL_AOV_GROUP = 1055640,
	DUMP_VALUE
};
