#pragma once
enum {
	Shader_Path = 0,
	SHADER_GROUP = 100,

	POPUP_INPUT_COLOR1,
	INPUT_COLOR1,
	INPUT_COLOR1_GROUP_PARAM,
	INPUT_COLOR1_SHADER,
	INPUT_COLOR1_SHADER_TEMP,

	POPUP_INPUT_IMPORTANCE1,
	INPUT_IMPORTANCE1,
	INPUT_IMPORTANCE1_GROUP_PARAM,
	INPUT_IMPORTANCE1_SHADER,
	INPUT_IMPORTANCE1_SHADER_TEMP,

	EMPTY1,

	POPUP_INPUT_COLOR2,
	INPUT_COLOR2,
	INPUT_COLOR2_GROUP_PARAM,
	INPUT_COLOR2_SHADER,
	INPUT_COLOR2_SHADER_TEMP,

	POPUP_INPUT_IMPORTANCE2,
	INPUT_IMPORTANCE2,
	INPUT_IMPORTANCE2_GROUP_PARAM,
	INPUT_IMPORTANCE2_SHADER,
	INPUT_IMPORTANCE2_SHADER_TEMP,

	EMPTY2,

	POPUP_INPUT_COLOR3,
	INPUT_COLOR3,
	INPUT_COLOR3_GROUP_PARAM,
	INPUT_COLOR3_SHADER,
	INPUT_COLOR3_SHADER_TEMP,

	POPUP_INPUT_IMPORTANCE3,
	INPUT_IMPORTANCE3,
	INPUT_IMPORTANCE3_GROUP_PARAM,
	INPUT_IMPORTANCE3_SHADER,
	INPUT_IMPORTANCE3_SHADER_TEMP,

	EMPTY3,

	POPUP_INPUT_COLOR4,
	INPUT_COLOR4,
	INPUT_COLOR4_GROUP_PARAM,
	INPUT_COLOR4_SHADER,
	INPUT_COLOR4_SHADER_TEMP,

	POPUP_INPUT_IMPORTANCE4,
	INPUT_IMPORTANCE4,
	INPUT_IMPORTANCE4_GROUP_PARAM,
	INPUT_IMPORTANCE4_SHADER,
	INPUT_IMPORTANCE4_SHADER_TEMP,

	EMPTY4,

	POPUP_INPUT_COLOR5,
	INPUT_COLOR5,
	INPUT_COLOR5_GROUP_PARAM,
	INPUT_COLOR5_SHADER,
	INPUT_COLOR5_SHADER_TEMP,

	POPUP_INPUT_IMPORTANCE5,
	INPUT_IMPORTANCE5,
	INPUT_IMPORTANCE5_GROUP_PARAM,
	INPUT_IMPORTANCE5_SHADER,
	INPUT_IMPORTANCE5_SHADER_TEMP,

	INPUT,
	RANDOM,
	SEED,
	DUMMY
};
